########################################
#### PROYECTO FINANZAS 2021 - LOGIT ####
########################################

############## Librerías a usar ##############
library(readxl)
library(dplyr)
library(ggplot2)
library(plotly)
library(klaR) # Para utilizar k-modes
library(igraph) # Análisis de Redes

library(intergraph) # Intermediario entre igraph - ggnetwork (redes con ggplot)
library(ggnetwork) # Redes con ggplot


##############################################     

# rm(list = ls()) 
# setwd("C:/Facultad/Proyectos IESTA/Finanzas/Finanzas 2021/Scripts")

# Limpieza de datos        

datos <- read_excel('Datos_Respuestas.xlsx', sheet = 'Codificado') # Se codificaron todas las preguntas para simplificar

faltantes <- apply(X = is.na(datos), MARGIN = 2, FUN = sum)
View(faltantes) # Hay faltantes en cantidad de personal y facturación de la empresa

datos <- datos[complete.cases(datos),] # Si quiero trabajar solo con datos completos (112 filas)

# La base tiene toda la información, armamos bloques para tener información agrupada

# Caracteristicas de la empresa
bloque_1 <- as.data.frame(datos[,3:11])
for (i in 1:length(bloque_1)) {
  bloque_1[,i] <- as.factor(bloque_1[,i])
}

# Situación de las empresas en declaratoria de emergencia
bloque_2 <- as.data.frame(datos[,12:19])
for (i in 1:length(bloque_2)) {
  bloque_2[,i] <- as.factor(bloque_2[,i])
}

# Medidas implementadas respecto a relaciones laborales
bloque_3 <- as.data.frame(datos[,20:33])
for (i in 1:length(bloque_3)) {
  bloque_3[,i] <- as.factor(bloque_3[,i])
}

# Cambios operativos en la empresa
bloque_4 <- as.data.frame(datos[,34:40])
for (i in 1:length(bloque_4)) {
  bloque_4[,i] <- as.factor(bloque_4[,i])
}

# Medidas implementadas respecto a las finanzas de la empresa
bloque_5 <- as.data.frame(datos[,41:52])
for (i in 1:length(bloque_5)) {
  bloque_5[,i] <- as.factor(bloque_5[,i])
}

# Analisis de variables financieras (Aca separamos las herramientas financieras de todos lo demas)
HF <- as.data.frame(datos[,78:86])
bloque_6 <- as.data.frame(datos[,c(53,55:60)])
for (i in 1:length(bloque_6)) {
  bloque_6[,i] <- as.factor(bloque_6[,i])
}

# Medidas de apoyo del gobierno
bloque_7 <- as.data.frame(datos[,61:69])
for (i in 1:length(bloque_7)) {
  bloque_7[,i] <- as.factor(bloque_7[,i])
}

# Medidas adoptadas que se mantendran en el futuro
bloque_8 <- as.data.frame(datos[,70:77])
for (i in 1:length(bloque_8)) {
  bloque_8[,i] <- as.factor(bloque_8[,i])
}

df_completo <- data.frame(bloque_1, bloque_2, bloque_3, bloque_4, bloque_5, bloque_6,
                          bloque_7, bloque_8, HF) # Aca tengo de nuevo la base pero con class factor y las dummies en herramientas financieras

# Resumen de bloques (Queda mas simple de manipular si se mete en una Shiny) 
# Bloque 1 - Caracteristicas de la empresa
# Bloque 2 - Situación de las empresas en declaratoria de emergencia
# Bloque 3 - Medidas implementadas respecto a relaciones laborales
# Bloque 4 - Cambios operativos en la empresa
# Bloque 5 - Medidas implementadas respecto a las finanzas de la empresa
# Bloque 6 - Analisis de variables financieras (Aca separamos las herramientas financieras de todos lo demas)
# Bloque 7 - Medidas de apoyo del gobierno
# Bloque 8 - Medidas adoptadas que se mantendran en el futuro
# HF - Herramientas Financieras (Para marcar perfiles de las empresas como trabajo anterior de kmodes)


### ANALISIS DESDE REGRESION LOGISTICA ###

# Lo estudiamos con todas las variables o por bloque?

# Por ejemplo Efecto positivo ~ Características de la empresa (Antiguedad_5 no es relevante, salvo 1 tienen todas + de 5 años)
df <- data.frame(bloque_1, bloque_2) # Bloque 1 - Caracteristicas | Bloque 2 - Situación empresas

modelo <- glm(efecto_positivo ~ participa_prop + educacion_ceo + actividad_emp + cant_personal + juridica_emp +
                factura_emp + exporta + familiar, data = df, family = "binomial")

summary(modelo) # Parece que exportación es una variable predictora significativa en el efecto positivo de la pandemia
step(modelo) # Hago un stepwise para elección de predictores (backward)
modelo_step <- glm(formula = efecto_positivo ~ cant_personal + juridica_emp + 
                     exporta, family = "binomial", data = df)
summary(modelo_step)

# **COMENTARIO** Empresas que exportan puede ser que hayan tenido un efecto positivo mayor con respecto a las que no exportan
# (Que tantas restricciones hubo en las exportaciones?)


# Otro ejemplo Teletrabajo ~ Caracteristicas de la empresa
modelo1 <- glm(teletrabajo ~ participa_prop + educacion_ceo + actividad_emp + cant_personal + juridica_emp +
                 factura_emp + exporta + familiar, data = df_completo, family = "binomial")

summary(modelo1)
step(modelo1)

modelo1_step <- glm(formula = teletrabajo ~ educacion_ceo + cant_personal + exporta + 
                      familiar, family = "binomial", data = df_completo) 

summary(modelo1_step)

# **COMENTARIO** (gran) cantidad de personal parece significativa a la hora de aplicar régimen de teletrabajo
#                Empresas que exportan tambien parece ser significativa -> Se trabaja mucho digitalmente (?)
#                Coeficiente negativo en empresas familiares, puede indicar que no optan por este régimen.

############################

# Que pasa con el uso de herramientas financieras? (Siguiendo línea de investigación del 2019)
# Debo ajusta un modelo por cada variable?

# HF ~ Características de la empresa

# Ratio de Endeudamiento
modeloHF1 <- glm(formula = Ratio_End ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF1)
step(modeloHF1)
modeloHF1_step <- glm(formula = Ratio_End ~ educacion_ceo + factura_emp, family = "binomial", 
                      data = df_completo)
summary(modeloHF1_step)

# Ratio de Liquidez
modeloHF2 <- glm(formula = Ratio_Liq ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF2)
step(modeloHF2)
modeloHF2_step <- glm(formula = Ratio_Liq ~ participa_prop + educacion_ceo + cant_personal, 
                      family = "binomial", data = df_completo)
summary(modeloHF2_step)

# Período promedio de cobranza
modeloHF3 <- glm(formula = PP_cob ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF3)
step(modeloHF3)
modeloHF3_step <- glm(formula = PP_cob ~ educacion_ceo + cant_personal + exporta, 
                      family = "binomial", data = df_completo)
summary(modeloHF3_step)

# Período promedio de cuentas a pagar
modeloHF4 <- glm(formula = PP_ctaspagar ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF4)
step(modeloHF4)
modeloHF4_step <- glm(formula = PP_ctaspagar ~ educacion_ceo, family = "binomial", 
                      data = df_completo)
summary(modeloHF4_step)

# Ratios de Rentabilidad
modeloHF5 <- glm(formula = Ratio_rent ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF5)
step(modeloHF5)
modeloHF5_step <- glm(formula = Ratio_rent ~ participa_prop + educacion_ceo + actividad_emp + 
                        cant_personal, family = "binomial", data = df_completo)
summary(modeloHF5_step)

# Resultado Operativo
modeloHF6 <- glm(formula = Res_Op ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF6)
step(modeloHF6)
modeloHF6_step <- glm(formula = Res_Op ~ educacion_ceo + cant_personal, family = "binomial", 
                      data = df_completo)
summary(modeloHF6_step)

# Presupuesto de Caja
modeloHF7 <- glm(formula = Presup_Caja ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF7)
step(modeloHF7)
modeloHF7_step <- glm(formula = Presup_Caja ~ cant_personal + factura_emp, family = "binomial", 
                      data = df_completo)
summary(modeloHF7_step)

# Control de inventarios
modeloHF8 <- glm(formula = Control_Inv ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF8)
step(modeloHF8)
modeloHF8_step <- glm(formula = Control_Inv ~ educacion_ceo + juridica_emp, family = "binomial", 
                      data = df_completo)
summary(modeloHF8_step)

# OTROS
modeloHF9 <- glm(formula = Otros ~ participa_prop + educacion_ceo + actividad_emp + 
                   cant_personal + juridica_emp + factura_emp + exporta + familiar, family = "binomial", data = df_completo)
summary(modeloHF9)
step(modeloHF9)
modeloHF9_step <- glm(formula = Otros ~ educacion_ceo, family = "binomial", data = df_completo)
summary(modeloHF9_step) 

# **COMENTARIO** El uso de las herramientas financieras está asociado a la educación del CEO (persona encuestada) principalmente
# En menor medida al tamaño de la empresa (cantidad de empleados) y facturación
# En el caso de herramientas financieras "OTROS" los coeficientes de la educación son negativos (hay menor chance de usar esas herramientas si la persona tiene cierto nivel educativo) 


# PROBLEMA DE ESTO: Un modelo por cada herramienta financiera (o variable), no es eficiente